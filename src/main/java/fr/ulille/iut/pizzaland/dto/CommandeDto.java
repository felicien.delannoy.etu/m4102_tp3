package fr.ulille.iut.pizzaland.dto;

import java.util.ArrayList;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Pizza;

public class CommandeDto {
	private String nom;
	private String prenom;
	private ArrayList<Pizza> pizzaList;
	private UUID id = UUID.randomUUID();
	
	public CommandeDto() {
		
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public ArrayList<Pizza> getPizzaList() {
		return pizzaList;
	}

	public void setPizzaList(ArrayList<Pizza> pizzaList) {
		this.pizzaList = pizzaList;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public CommandeDto(String nom, String prenom, ArrayList<Pizza> pizzaList) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.pizzaList = pizzaList;
	}
}
