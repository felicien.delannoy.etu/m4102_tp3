package fr.ulille.iut.pizzaland.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
	
  public interface PizzaDao {
	
    @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas(id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
    void createPizzaTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation(pizzaID VARCHAR(128), ingredientID VARCHAR(128), PRIMARY KEY(pizzaID,ingredientID))")
    void createAssociationTable();

    @Transaction
    default void createTableAndIngredientAssociation() {
      createAssociationTable();
      createPizzaTable();
    }
    
    
    @SqlUpdate("DROP TABLE IF EXISTS pizzas")
    void dropTable();
    
    @SqlQuery("SELECT * FROM Pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();
    
    @SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(@Bind("id") UUID id);

	@SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
	Pizza findByName(@Bind("name")String name);
	
    @SqlQuery("Select * from ingredients where id in(SELECT ingredientID FROM PizzaIngredientsAssociation WHERE pizzaID = :id)")
    @RegisterBeanMapper(Ingredient.class)
   List<Ingredient> findIngredientsById(@Bind("id") UUID id);
    
    @SqlUpdate("INSERT INTO PizzaIngredientsAssociation (pizzaID,ingredientID) VALUES( :pizzaID, :ingredientID)")
    void insertPizzaAndIngredientAssociation(@Bind("pizzaID") UUID pizzaID ,@Bind("ingredientID") UUID ingredientID);
    
    @SqlUpdate("INSERT INTO Pizzas (id, name) VALUES (:id, :name)")
    void insert(@BindBean Pizza pizza);

    @SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
    void remove(@Bind("id") UUID id);  
    
    default void insertTablePizzaAndIngredientAssociation(Pizza pizza) {
    	this.insert(pizza);
    	for(Ingredient ingr : pizza.getIngredients()) {
    		this.insertPizzaAndIngredientAssociation(pizza.getId(),ingr.getId());
    	}
    }


	default void deleteIngredientFromPizza(UUID pizzaId, UUID ingredientId) {
		this.deleteIngredientFromPizzaAssociation(pizzaId,ingredientId);
		this.deleteIngredient(ingredientId);
	}
	@SqlUpdate("Delete FROM ingredients where id = :ingredientId")
	void deleteIngredient(UUID ingredientId);

	@SqlUpdate("Delete FROM PizzaIngredientsAssociation where pizzaID = :pizzaID AND ingredientID = :ingredientID")
	void deleteIngredientFromPizzaAssociation(@Bind("pizzaID") UUID pizzaID, @Bind ("ingredientID") UUID ingredientID);

	
  }
