package fr.ulille.iut.pizzaland.dao;

import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface CommandeDao {

	@Transaction
	default void createTableAndPizzaAssociation() {
		createAssociationTable();
		createCommandeTable();
	}
	@SqlUpdate("CREATE TABLE IF NOT EXISTS Commandes(id VARCHAR(128) PRIMARY KEY, nom VARCHAR UNIQUE NOT NULL,prenom VARCHAR UNIQUE NOT NULL)")
	void createCommandeTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS CommandesPizzasAssociation(commandeID VARCHAR(128), pizzaID VARCHAR(128), PRIMARY KEY(commandeID,pizzaID))")
	void createAssociationTable();



	@SqlUpdate("DROP TABLE IF EXISTS PizzaCommandeAssociation")
	void dropTable();

	@SqlUpdate("DROP TABLE IF EXISTS Commande")
	void dropCommandeTable();

	@SqlUpdate("INSERT INTO Commande (id, nom, prenom) VALUES (:id, :nom, :prenom)")
	void insert(@BindBean Commande commande);

	@SqlUpdate("INSERT INTO PizzaCommandeAssociation (commandeID,pizzaID) VALUES(:commande.id, :pizza.id)")
	void insertPizzaAssociation(@BindBean("pizza") Pizza pizza ,@BindBean("commande") Commande commande);

	@Transaction
	default void dropCommandeAndPizzaAssociation() {
		dropCommandeTable();
		dropTable();
	}
}
