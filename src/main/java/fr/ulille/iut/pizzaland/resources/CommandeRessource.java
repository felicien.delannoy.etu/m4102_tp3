package fr.ulille.iut.pizzaland.resources;

import java.util.logging.Logger;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDAO;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;

@Path("/ingredients")
@Produces("application/json")
public class CommandeRessource {
	private static final Logger LOGGER = Logger.getLogger(CommandeRessource.class.getName());

	private CommandeDao commandes;

	@Context
	public UriInfo uriInfo;

	public CommandeResource() {
		commandes = BDDFactory.buildDao(IngredientDAO.class);
		commandes.createTable();
	}

}
